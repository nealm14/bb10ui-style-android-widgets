package org.bbui.nm;

import android.app.TabActivity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TabHost;
import android.widget.TextView;


public class TabhostActivity extends TabActivity{
	private TabHost tabHost;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tabhost);
        setTabs();
    }
    
    private void setTabs() {
		tabHost = getTabHost();
		

		addTab(R.string.tabOne, R.drawable.tab_info);
		addF1Tab(R.string.tabTwo, R.drawable.tab_info);
		addWsbkTab(R.string.tabThree, R.drawable.tab_info);
		addBsbTab(R.string.tabFour, R.drawable.tab_info);
		addBtccTab(R.string.tabFive, R.drawable.tab_info);
	}
	

private void addTab(int labelId, int drawableId) {
	Intent intent = new Intent(this, TabOne.class);
	TabHost.TabSpec spec = tabHost.newTabSpec("tab" + labelId);		
	
	View tabIndicator = LayoutInflater.from(this).inflate(R.layout.tab_indicator, getTabWidget(), false);
	
	TextView title = (TextView) tabIndicator.findViewById(R.id.title);
	title.setText(labelId);
	Typeface font = Typeface.createFromAsset(getAssets(), "MYRIADPRO-REGULAR.OTF");  
	title.setTypeface(font); 
	spec.setIndicator(tabIndicator);
	spec.setContent(intent);
	tabHost.addTab(spec);
}

private void addF1Tab(int labelId, int drawableId) {
	Intent intent = new Intent(this, TabTwo.class);
	TabHost.TabSpec spec = tabHost.newTabSpec("tab" + labelId);		
	View tabIndicator = LayoutInflater.from(this).inflate(R.layout.tab_indicator, getTabWidget(), false);
	TextView title = (TextView) tabIndicator.findViewById(R.id.title);
	title.setText(labelId);
	Typeface font = Typeface.createFromAsset(getAssets(), "MYRIADPRO-REGULAR.OTF");  
	title.setTypeface(font); 
	spec.setIndicator(tabIndicator);
	spec.setContent(intent);
	tabHost.addTab(spec);
}

private void addWsbkTab(int labelId, int drawableId) {
	Intent intent = new Intent(this, TabThree.class);
	TabHost.TabSpec spec = tabHost.newTabSpec("tab" + labelId);		
	View tabIndicator = LayoutInflater.from(this).inflate(R.layout.tab_indicator, getTabWidget(), false);
	TextView title = (TextView) tabIndicator.findViewById(R.id.title);
	title.setText(labelId);
	Typeface font = Typeface.createFromAsset(getAssets(), "MYRIADPRO-REGULAR.OTF");  
	title.setTypeface(font); 
	spec.setIndicator(tabIndicator);
	spec.setContent(intent);
	tabHost.addTab(spec);
}

private void addBsbTab(int labelId, int drawableId) {
	Intent intent = new Intent(this, TabFour.class);
	TabHost.TabSpec spec = tabHost.newTabSpec("tab" + labelId);		
	View tabIndicator = LayoutInflater.from(this).inflate(R.layout.tab_indicator, getTabWidget(), false);
	TextView title = (TextView) tabIndicator.findViewById(R.id.title);
	title.setText(labelId);
	Typeface font = Typeface.createFromAsset(getAssets(), "MYRIADPRO-REGULAR.OTF");  
	title.setTypeface(font); 
	spec.setIndicator(tabIndicator);
	spec.setContent(intent);
	tabHost.addTab(spec);
}

private void addBtccTab(int labelId, int drawableId) {
	Intent intent = new Intent(this, TabFive.class);
	TabHost.TabSpec spec = tabHost.newTabSpec("tab" + labelId);		
	View tabIndicator = LayoutInflater.from(this).inflate(R.layout.tab_indicator, getTabWidget(), false);
	TextView title = (TextView) tabIndicator.findViewById(R.id.title);
	title.setText(labelId);
	Typeface font = Typeface.createFromAsset(getAssets(), "MYRIADPRO-REGULAR.OTF");  
	title.setTypeface(font); 
	spec.setIndicator(tabIndicator);
	spec.setContent(intent);
	tabHost.addTab(spec);
}


}