package org.bbui.nm;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class TabOne extends Activity {
	
	@Override 
	public void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tabone);
		
		Typeface font = Typeface.createFromAsset(getAssets(), "MYRIADPRO-REGULAR.OTF");  
		
		
		TextView largeText = (TextView) findViewById(R.id.largeText);
		TextView mediumText = (TextView) findViewById(R.id.mediumText);
		TextView smallText = (TextView) findViewById(R.id.smallText);
		
		Button smallButton = (Button) findViewById(R.id.smallButton);
		Button toggleButton = (Button) findViewById (R.id.toggleButton);
		Button largeButton = (Button) findViewById(R.id.largeButton);
		
		CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox);
		
		RadioButton radioButton = (RadioButton) findViewById(R.id.radioButton1);
		
		RadioButton radioGroupButton0 = (RadioButton) findViewById(R.id.radio0); //RadioGroup buttons 
		RadioButton radioGroupButton1 = (RadioButton) findViewById(R.id.radio1); //RadioGroup buttons 
		RadioButton radioGroupButton2 = (RadioButton) findViewById(R.id.radio2); //RadioGroup buttons 
		
	
		largeText.setTypeface(font); 
		mediumText.setTypeface(font); 
		smallText.setTypeface(font); 
		
		smallButton.setTypeface(font);
		toggleButton.setTypeface(font);
		largeButton.setTypeface(font);
		
		checkBox.setTypeface(font);
		
		radioButton.setTypeface(font);
		
		radioGroupButton0.setTypeface(font);
		radioGroupButton1.setTypeface(font);
		radioGroupButton2.setTypeface(font);
	}
	
}