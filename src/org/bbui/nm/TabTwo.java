package org.bbui.nm;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.EditText;

public class TabTwo extends Activity {
	
	@Override 
	public void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tabtwo);
		
		Typeface font = Typeface.createFromAsset(getAssets(), "MYRIADPRO-REGULAR.OTF");  
		
		EditText editText1 = (EditText) findViewById(R.id.editText1);
		EditText editText2 = (EditText) findViewById(R.id.editText2);
		EditText editText3 = (EditText) findViewById(R.id.editText3);
		EditText editText4 = (EditText) findViewById(R.id.editText4);
		EditText editText5 = (EditText) findViewById(R.id.editText5);

		editText1.setTypeface(font);
		editText2.setTypeface(font);
		editText3.setTypeface(font);
		editText4.setTypeface(font);
		editText5.setTypeface(font);
		
		editText1.setText("Experiment. You wont break anything!");
		
	}
	
}