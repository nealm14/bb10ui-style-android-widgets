package org.bbui.nm;



import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends Activity {

	
	@Override 
	public void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.startupscreen);
		TextView txt = (TextView) findViewById(R.id.logo_text);  
		Typeface font = Typeface.createFromAsset(getAssets(), "MYRIADPRO-REGULAR.OTF");  
		txt.setTypeface(font);  
		
		Thread screenThread = new Thread() {
			@Override
			public void run() {
				try {
					int waited = 0;
					while (waited < 3000) {
						sleep(100);
						waited += 100;
					}
				} catch (InterruptedException e) {
					
				} finally {
					finish();
					
					Intent i = new Intent();
					i.setClassName("org.bbui.nm", "org.bbui.nm.TabhostActivity");
					startActivity(i);
				}
			}
		};
		screenThread.start();
	}
	
}	
